CREATE table goods(
    id INT(11) PRIMARY KEY  AUTO_INCREMENT,
    name VARCHAR(255) not null,
    price INT(11) not null,
    category VARCHAR(255) not null 
);

INSERT INTO goods VALUES
(null,"茄子",8,"蔬菜"),
(null,"橙子",6,"水果"),
(null,"血橙",10,"水果"),
(null,"苹果",5,"水果"),
(null,"白菜",7,"蔬菜"),
(NULL,"西瓜",15,"水果"),
(null,"火龙果",20,"水果"),
(null,"丝瓜",4,"蔬菜"),
(null,"南瓜",1,"蔬菜"),
(null,"柚子",2,"水果")



-- 1.查询表所有信息
SELECT * FROM goods;
-- 2.查询name和price字段
SELECT name , price FROM goods;
-- 3.查询前3条信息
SELECT * FROM goods LIMIT 3;
-- 4.查询从下标3开始的5条信息
SELECT * from goods LIMIT 3,5;
-- 5.查询商品价格的最大值
SELECT max(price) AS 商品价格最大值 FROM goods;
-- 6.查询商品价格最小值
SELECT min(price) as 商品价格最小值 from goods;
-- 7.查询最高价格是最低价格的几倍 
SELECT max(price) / min(price)  from goods;
-- 8.查询类型分组的个数
SELECT category FROM goods GROUP BY;
-- 9查询类型分组个数
SELECT category,count(*) from goods GROUP BY category;
-- 10.查看价格的平均值
SELECT avg(price) FROM goods;
-- 11.模糊查询含有瓜的商品
SELECT name FROM goods WHERE name like "%瓜%";
-- 12.修改南瓜的价格为100
UPDATE goods set price = 100 WHERE name = "南瓜";
-- 13.删除id为9和10的商品
delete from goods WHERE id in (9,10);
-- 14.添加类型：其他 name 小傻瓜 价格 0
INSERT INTO  goods VALUES
(null,"小傻瓜",0,"其他");
-- 15.价格按照由高到低排序
SELECT price FROM goods ORDER BY price DESC;