--  统一提交到组长的 gitee 上 
--  交作业，组长gitee 仓库地址
--  创建一张表 goods
--  id     name    price    category
--  1      茄子     8        蔬菜
--  2      橙子     6        水果 
--  3      血橙     10       水果
--  4      苹果     5        水果
--  5      白菜     7        蔬菜
--  6      西瓜     15       水果
--  7      火龙果   20       水果
--  8      丝瓜     4        蔬菜
--  9      南瓜     1        蔬菜
--  10     柚子     2        水果
;
CREATE TABLE goods(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(11) NOT NULL,
    price DOUBLE(11, 2),
    category VARCHAR(11) NOT NULL
);
INSERT INTO goods (id, name, price, category)
VALUES (null, "茄子", 8, "蔬菜"),
    (null, "橙子", 6, "水果"),
    (null, "血橙", 10, "水果"),
    (null, "苹果", 5, "水果"),
    (null, "白菜", 7, "蔬菜"),
    (null, "西瓜", 15, "水果"),
    (null, "火龙果", 20, "水果"),
    (null, "丝瓜", 4, "蔬菜"),
    (null, "南瓜", 1, "蔬菜"),
    (null, "柚子", 2, "水果");
-- 查询该表的所有信息
SELECT *
FROM goods;
-- 查询 name 和 price字段
SELECT name,
    price
FROM goods;
-- 查询前三条信息
SELECT *
FROM goods
LIMIT 3;
-- 查询从下标3开始的5条信息
SELECT *
FROM goods
LIMIT 3, 5;
-- 查询商品价格的最大值
SELECT MAX(price) AS 最大值
FROM goods;
-- 查询商品价格的最小值
SELECT MIN(price) AS 最小值
FROM goods;
-- 查询 最高价格是最低价格的几倍
SELECT MAX(price) / MIN(price) AS 几倍
FROM goods;
-- 查询类型分组个数
SELECT category,
    COUNT(*) AS 数量
FROM goods
GROUP BY category;
-- 查询价格平均值
SELECT AVG(price) AS 平均数
FROM goods;
-- 模糊查询含有瓜的商品
SELECT *
FROM goods
WHERE name LIKE "%瓜%";
-- 修改南瓜的价格为100
UPDATE goods
SET price = 100
WHERE id = 9;
-- 删除 id 为 9 和 10 的商品
DELETE FROM goods
WHERE id IN(9, 10);
-- 添加 类型：其他  小傻瓜 价格 0
INSERT INTO goods (id, name, price, category)
VALUES (null, "小傻瓜", 0, "其他");
-- 按照价格又高到低排序
SELECT *
FROM goods
ORDER BY price DESC;