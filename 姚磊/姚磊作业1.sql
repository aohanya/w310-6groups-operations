CREATE TABLE goods(
    id int primary key auto_increment,
    name varchar(20) not null,
    price float(11, 2) not null,
    category varchar(20) not null
);
INSERT into goods VALUE (null, "茄子", 8, "蔬菜"),
    (null, "橙子", 6, "水果"),
    (null, "血橙", 10, "水果"),
    (null, "苹果", 5, "水果"),
    (null, "白菜", 7, "蔬菜"),
    (null, "西瓜", 15, "水果"),
    (null, "丝瓜", 4, "蔬菜"),
    (null, "南瓜", 1, "蔬菜"),
    (null, "柚子", 2, "水果"),
    (null, "火龙果", 20, "水果");
-- 查询该表的全部信息
SELECT *
FROM goods;
-- 查询 name 和 price字段
SELECT name,
    price
FROM goods;
-- 查询前三条信息
SELECT *
FROM goods
LIMIT 3;
-- 查询从下标3开始到下标5的信息
SELECT *
FROM goods
LIMIT 3, 5;
-- 查询商品价格的最大值
SELECT max(price) AS 价格最大值
FROM goods;
-- 查询商品价格的最小值
SELECT min(price) AS 价格最小值
FROM goods;
-- 查询最低价格是最高价格的几倍
SELECT max(price) / min(price)
FROM goods;
-- 查询类型分组个数
SELECT category AS 类型,
    count(category) AS 数量
FROM goods
GROUP BY category;
-- 查询价格平均值
SELECT avg(price)
FROM goods;
-- 查询名称 含有 瓜 的商品
SELECT *
FROM goods
WHERE name like "%瓜%";
-- 修改南瓜的价格为100
UPDATE goods
set price = 100
WHERE name = "南瓜";
-- 删除 id为 9 和 10 的商品
DELETE FROM goods
WHERE id in(9, 10);
-- 添加 类型：其他  小傻瓜 价格 0
INSERT INTO goods
VALUES(NULL, "小傻瓜", 0, "其他");
-- 按照价格由高到低排序
SELECT *
FROM goods
ORDER BY price DESC;