/* 
统一提交到组长的 gitee 上 
交作业，组长gitee 仓库地址

创建一张表 goods

id     name    price    category
1      茄子     8        蔬菜
2      橙子     6        水果 
3      血橙     10       水果
4      苹果     5        水果
5      白菜     7        蔬菜
6      西瓜     15       水果
7      火龙果   20       水果
8      丝瓜     4        蔬菜
9      南瓜     1        蔬菜
10     柚子     2        水果
 */

CREATE TABLE foods (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(13) NOT null,
    price DOUBLE(13, 2) NOT NULL,
    category VARCHAR(13) NOT NULL
);

INSERT INTO foods VALUES(NULL, "茄子", 8, "蔬菜"),
(NULL, "橙子", 6, "水果"),
(NULL, "血橙", 10, "水果"),
(NULL, "苹果", 5, "水果"),
(NULL, "白菜", 7, "蔬菜"),
(NULL, "西瓜", 15, "水果"),
(NULL, "火龙果", 20, "水果"),
(NULL, "丝瓜", 4, "蔬菜"),
(NULL, "南瓜", 1, "蔬菜"),
(NULL, "柚子", 2, "水果");

-- 查询该表的所有信息
SELECT * FROM foods;
-- 查询 name 和 price字段
SELECT name,price FROM foods;
-- 查询前三条信息
SELECT * FROM foods LIMIT 3;
-- 查询从下标3开始的5条信息
SELECT * FROM foods LIMIT 3,5;
-- 查询商品价格的最大值
SELECT MAX(price) FROM foods;
-- 查询商品价格的最小值
SELECT MIN(price) FROM foods;
-- 查询 最高价格是最低价格的几倍
SELECT MAX(price) / MIN(price) FROM foods;
-- 查询类型分组个数
SELECT category, COUNT(*) AS "总数" FROM foods GROUP BY category;
-- 查询价格平均值
SELECT AVG(price) AS "平均价格" FROM foods;
-- 模糊查询含有瓜的商品
SELECT * FROM foods WHERE name like "%瓜%";
-- 修改南瓜的价格为100
UPDATE foods SET price=100 WHERE name="南瓜";
-- 删除 id 为 9 和 10 的商品
DELETE FROM foods WHERE id=9 or id=10;
-- 添加 类型：其他  小傻瓜 价格 0
INSERT INTO foods(id, name, price, category) VALUES(NULL, "小傻瓜", 0, "其他");
-- 按照价格由高到低排序
SELECT * FROM foods ORDER BY price DESC;