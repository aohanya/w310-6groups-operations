CREATE TABLE good(
    id int primary key auto_increment,
    name varchar(20) not null,
    price float(11, 2) not null,
    category varchar(20) not null
);
INSERT into good VALUE (null, "茄子", 8, "蔬菜"),
    (null, "橙子", 6, "水果"),
    (null, "血橙", 10, "水果"),
    (null, "苹果", 5, "水果"),
    (null, "白菜", 7, "蔬菜"),
    (null, "西瓜", 15, "水果"),
    (null, "丝瓜", 4, "蔬菜"),
    (null, "南瓜", 1, "蔬菜"),
    (null, "柚子", 2, "水果"),
    (null, "火龙果", 20, "水果");
-- 查询该表的全部信息
SELECT *
FROM good;
-- 查询 name 和 price字段
SELECT name,
    price
FROM good;
-- 查询前三条信息
SELECT *
FROM good
LIMIT 3;
-- 查询从下标3开始的五条信息
SELECT *
FROM good
LIMIT 3, 5;
-- 查询商品价格的最大值
SELECT max(price) AS 价格最大值
FROM good;
-- 查询商品价格的最小值
SELECT min(price) AS 价格最小值
FROM good;
-- 查询最低价格是最高价格的几倍
SELECT max(price) / min(price)
FROM good;
-- 查询类型分组个数
SELECT category AS 类型,
    count(*) AS 数量
FROM good
GROUP BY category;
-- 查询价格平均值
SELECT avg(price)
FROM good;
-- 查询名称 含有 瓜 的商品
SELECT *
FROM good
WHERE name like "%瓜%";
-- 修改南瓜的价格为100
UPDATE good
set price = 100
WHERE name = "南瓜";
-- 删除 id为 9 和 10 的商品
DELETE FROM good
WHERE id in(9, 10);
-- 添加 类型：其他  小傻瓜 价格 0
INSERT INTO good
VALUES(NULL, "小傻瓜", 0, "其他");
-- 按照价格由高到低排序
SELECT *
FROM good
ORDER BY price DESC;