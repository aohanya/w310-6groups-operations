-- 创建foods表
CREATE TABLE foods (
    id INT primary key auto_increment,
    name VARCHAR(30) NOT NULL,
    price DOUBLE(11, 2) NOT NULL,
    category VARCHAR(30) NOT NULL
);
-- foods表里插入内容
INSERT INTO foods
VALUES (NULL, "茄子", 8, "蔬菜"),
    (NULL, "橙子", 6, "水果"),
    (NULL, "血橙", 10, "水果"),
    (NULL, "苹果", 5, "水果"),
    (NULL, "白菜", 7, "蔬菜"),
    (NULL, "西瓜", 15, "水果"),
    (NULL, "火龙果",20, "水果"),
    (NULL, "丝瓜", 4, "蔬菜"),
    (NULL, "南瓜", 1, "蔬菜"),
    (NULL, "柚子", 2, "水果");
-- 查询该表的全部信息
SELECT * FROM foods;

-- 查询 name 和 price字段
SELECT name,price FROM foods;

-- 查询前三条信息
SELECT * FROM  foods LIMIT 3;

-- 查询从下标3开始到下标5的信息
SELECT * FROM foods LIMIT 3,5;

-- 查询商品价格的最大值
SELECT *,max(price) AS 最大值 FROM foods;

-- 查询商品价格的最小值
SELECT *,min(price) AS 最小值 FROM foods;

-- 查询最低价格是最高价格的几倍
SELECT max(price)/min(price) FROM foods;

-- 查询类型分组个数
SELECT category,count(category) FROM foods GROUP BY category;

-- 查询价格平均值
SELECT avg(price) FROM foods;

-- 查询名称 含有 瓜 的商品
SELECT * FROM foods WHERE name like "%瓜%";

-- 修改南瓜的价格为100
UPDATE foods set price=100 WHERE name="南瓜";

-- 删除 id为 9 和 10 的商品
DELETE FROM foods WHERE id in(9,10);

-- 添加 类型：其他  小傻瓜 价格 0
INSERT INTO foods VALUES(NULL,"小傻瓜",0,"其他");

-- 按照价格由高到低排序
SELECT * FROM foods ORDER BY price DESC;