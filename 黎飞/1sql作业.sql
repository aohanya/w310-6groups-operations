-- 创建一张表 goods
-- id     name    price    category
-- 1      茄子     8        蔬菜
-- 2      橙子     6        水果 
-- 3      血橙     10       水果
-- 4      苹果     5        水果
-- 5      白菜     7        蔬菜
-- 6      西瓜     15       水果
-- 7      火龙果   20       水果
-- 8      丝瓜     4        蔬菜
-- 9      南瓜     1        蔬菜
-- 10     柚子     2        水果
--商品类型
create table category(
    id int primary key auto_increment,
    name varchar(30) not null
);
--商品表
create table goods(
    id int primary key auto_increment,
    --商品名
    name varchar(30) not null,
    --价格
    price int(11, 2) NOT NULL,
    --类型
    category varchar(30) not null,
    --类型ID
    category_id int,
    foreign key(category_id) references category(id)
);
insert into category(name)
values("蔬菜"),
    ("水果") ("其他");
insert into goods
values (null, "茄子", 8, "蔬菜", 1),
    (null, "橙子", 6, "水果", 2),
    (null, "血橙", 10, "水果", 2),
    (null, "苹果", 5, "水果", 2),
    (null, "白菜", 7, "蔬菜", 1),
    (null, "西瓜", 15, "水果", 2),
    (null, "火龙果", 20, "水果", 2),
    (null, "丝瓜", 4, "蔬菜", 1),
    (null, "南瓜", 1, "蔬菜", 1),
    (null, "柚子", 2, "水果", 2);
-- 查询该表的所有信息
select *
from goods;
-- 查询 name 和 price字段
select name,
    price
from goods;
-- 查询前三条信息
select *
from goods
limit 3;
-- 查询从下标3开始的5条信息
select *
from goods
limit 3, 5;
-- 查询商品价格的最大值
select max(price)
from goods;
-- 查询商品价格的最小值
select min(price)
from goods;
-- 查询 最高价格是最低价格的几倍
select max(price) / min(price)
from goods;
-- 查询类型分组个数
select name as "商品类型",
    count(*) as "数量"
from goods
GROUP BY category;
-- 查询价格平均值
select avg(price)
from goods;
-- 模糊查询含有瓜的商品
select *
from goods
where name like "%瓜%";
-- 修改南瓜的价格为100
update goods
set price = 100
where id = 9;
-- 删除 id 为 9 和 10 的商品
delete from goods
where id in(9, 10);
-- 添加 类型：其他  小傻瓜 价格 0
insert into goods
values(null, "小傻瓜", 0, "其他");
-- 按照价格又高到低排序
select *
from goods
ORDER BY price DESC;select * from goods ORDER BY price DESC;